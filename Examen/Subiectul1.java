package examen.s1;

interface Y {

    public void f();
}

abstract class I implements Y {
    private long t;

    @Override
    public void f() {

    }
}

class J {

    public void i(I l) {

    }

}

class K {

    private M y;


    class L {
        public void metA() {

        }
    }
}

class M {
    public void metB() {
    }
}


class N {


}
public class Subiectul1{
    public static void main (String [] args){

    }
}